#!/usr/local/bin/python3

from flask import Flask
import pygal
import re

app = Flask(__name__)
data = {}

def add_obj(chan, t, val):
    if chan in data:
        data[chan].append((t,val))
    else:
        data[chan]=[(t,val)]

@app.route("/")
def index():
    data.clear()
    xy = pygal.XY()
    f=open('/Users/fsedano/gitlab/pid_controller/bld/dump.txt', 'r')
    for l in f:
        v=re.split("([\w/-]+)", l.rstrip())
        if len(v) == 7:
            add_obj(v[1], int(v[3]), int(v[5]))
        else:
            print("Invalid line {}".format(l))
    for k in data.keys():
        print("Adding graph {}".format(k))
        xy.add(k, data[k])
    return xy.render_response()

if __name__ == "__main__":
    app.run(debug=True)