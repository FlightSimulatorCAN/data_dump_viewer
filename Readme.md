Simple data visualization for raw flight sim frames

Start dump with fdump, data will be written to /mnt/dump.txt


```
*** Flight simulator CAN Gateway ***

fscbus_controller> fdump 19/3 1
Flags now 0x5

fscbus_controller> fdump 19/0 1
Flags now 0x5

fscbus_controller> show chan
 Available channels:
* : Dumping data
 Type          Slot/Port          Status   Value    Lua CB              Description
 ----------------------------------------------------------------------------------
*Adc            19/0              R        640                          Yoke pitch
 PWM            19/2              W        0                            Pitch force
*Adc            19/3              R        -3360                        Yoke Roll
 PWM            19/4              W        0                            Pitch trim
 PWM            19/5              W        0                            elastic K trim
 PWM            19/6              W        0                            press K
 PWM            19/7              W        0                            press zero
 PWM            19/8              W        0                            Sim force
 Rele           19/9              W        0                            Motor enable
 Rele           19/10             W        0                            Invert force sensor
 Adc            19/91             R        0                            Force sensor
 Adc            19/92             R        0                            PWM out
 Rele           19/93             W        0                            Pulse pull
 Rele           19/94             W        0                            Pulse push
 ----------------------------------------------------------------------------------
 14 available channel(s) (14 online), 1 cards
```

![Alt text](/screen_cap.png?raw=true "Screen cap")

